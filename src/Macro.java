import org.json.simple.JSONObject;

public class Macro {
    public int key;
    public String action;
    public String type;

    public Macro(int key, String action, String type) {
        this.key = key;
        this.action = action;
        this.type = type;
    }

    public JSONObject convertToJson()
    {
        JSONObject jsObj = new JSONObject();
        jsObj.put("key", key);
        jsObj.put("action", action);
        jsObj.put("type", type);

        return jsObj;
    }
}
