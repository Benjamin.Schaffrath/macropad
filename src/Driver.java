import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Driver {
    public Config config = null;
    public HashMap<String, String> mappingTable = new HashMap<>();
    public Robot robot;

    public Driver (Config config) {
        this.config = config;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        setupMappingTable();

        try {
            GlobalScreen.registerNativeHook();
            Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
            logger.setLevel(Level.WARNING);
            logger.setUseParentHandlers(false);
        } catch (NativeHookException e) {
            e.printStackTrace();
        }

        GlobalScreen.addNativeKeyListener(new NativeKeyListener() {
            @Override
            public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
                try {
                    handleMacro(nativeKeyEvent.getRawCode(), nativeKeyEvent.getModifiers());
                } catch (AWTException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {

            }

            @Override
            public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {

            }
        });
    }

    public void setupMappingTable() {
        Map table = mappingTable;

        //Normal Mode
        table.put("124_24", "1");
        table.put("125_24", "2");
        table.put("126_24", "3");
        table.put("127_24", "4");
        table.put("128_24", "5");
        table.put("129_24", "6");
        table.put("130_24", "7");
        table.put("131_24", "8");
        table.put("132_24", "9");
        table.put("133_24", "10");
        table.put("134_24", "11");
        table.put("135_24", "12");
        table.put("124_26", "13");
        table.put("125_26", "14");
        table.put("126_26", "15");
        table.put("127_26", "16");

        //Double Mode
        table.put("124_25", "1");
        table.put("125_25", "2");
        table.put("126_25", "3");
        table.put("127_25", "4");
        table.put("128_25", "5");
        table.put("129_25", "6");
        table.put("130_25", "7");
        table.put("131_25", "8");
        table.put("132_25", "9");
        table.put("133_25", "10");
        table.put("134_25", "11");
        table.put("135_25", "12");
        table.put("124_27", "13");
        table.put("125_27", "14");
        table.put("126_27", "15");
        table.put("127_27", "16");

        //Sound
        table.put("49_9", "0");
        table.put("50_9", "6");
        table.put("51_9", "12");
        table.put("52_9", "18");
        table.put("53_9", "24");
        table.put("54_9", "32");
        table.put("55_9", "38");
        table.put("56_9", "44");
        table.put("57_9", "50");
        table.put("48_9", "56");
        table.put("49_11", "62");
        table.put("50_11", "68");
        table.put("51_11", "74");
        table.put("52_11", "80");
        table.put("53_11", "86");
        table.put("54_11", "92");
        table.put("55_11", "100");

    }

    public void handleMacro(int keyCode, int modifier) throws AWTException, IOException {
        int mode = 0;
        Macro macro = null;

        if (modifier == 24 || modifier == 26) {
            //Handle normal mode
            mode = 1;
        }else if(modifier == 25 || modifier == 27) {
            //Handle double mode
            mode = 2;

        }else if(modifier == 9 || modifier == 11) {
            //Handle Sound
            mode = 3;
        }else{
            return;
        }

        String key = mappingTable.get(keyCode + "_" + modifier);
        if (key != null) {
            switch (mode) {
                case 1:
                    macro = (Macro) config.standardConfig.get(Integer.parseInt(key) -1);
                    break;
                case 2:
                    macro = (Macro) config.doubleConfig.get(Integer.parseInt(key) -1);
                    break;
                case 3:
                    VolumeControl.setSystemVolume(Integer.parseInt(key));
                    break;
            }

            if (mode != 3){
                switch (macro.type){

                    case "text":
                        executeText(macro.action);
                        break;
                    case "keys":
                        executeCombo(macro.action);
                        break;
                    case "exePath":
                        executePath(macro.action);
                        break;
                }
            }
        }
    }

    public void executeText(String text) throws AWTException {
        StringSelection stringSelection = new StringSelection(text);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, stringSelection);

        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
    }

    public void executeCombo(String c) {
        String[] combos = c.split(";");
        for (String combo : combos) {
            String[] parts = combo.split("\\|");
            ArrayList modifiers = splitModifiers(Integer.parseInt(parts[0]));
            String key = parts[1];

            try{
                for (Object modifier : modifiers) {
                    robot.keyPress(Integer.parseInt(modifier.toString()));
                }

                robot.keyPress(Integer.parseInt(key));
                robot.keyRelease(Integer.parseInt(key));

                for (Object modifier : modifiers) {
                    robot.keyRelease(Integer.parseInt(modifier.toString()));
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    public void executePath(String path) throws IOException {
        File file = new File(path);
        Desktop.getDesktop().open(file);
    }

    public ArrayList splitModifiers(int modifiers){
        ArrayList results = new ArrayList();

        if ((modifiers & InputEvent.SHIFT_DOWN_MASK) != 0 ) {
            results.add("16");
        }

        if ((modifiers & InputEvent.CTRL_DOWN_MASK) != 0 ) {
            results.add("17");
        }

        if ((modifiers & InputEvent.META_DOWN_MASK) != 0 ) {
            results.add("157");
        }

        if ((modifiers & InputEvent.ALT_DOWN_MASK) != 0 ) {
            results.add("18");
        }

        if ((modifiers & InputEvent.ALT_GRAPH_DOWN_MASK) != 0 ) {
            results.add("65406");
        }

        return results;
    }
}
