import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Config {
    public List standardConfig = new ArrayList<Macro>();
    public List doubleConfig = new ArrayList<Macro>();

    public Config()  {
        for (int i = 1; i <= 16; i++){
            this.standardConfig.add(new Macro(i, "", ""));
            this.doubleConfig.add(new Macro(i, "", ""));
        }
        try {
            this.LoadFromConfigFile();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public void LoadFromConfigFile() throws IOException, org.json.simple.parser.ParseException {

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(System.getProperty("user.dir") + "\\config.json"));
        JSONObject JSONObject = (JSONObject) obj;
        JSONArray standardConfigs = (JSONArray) JSONObject.get("standardConfig");
        JSONArray doubleConfigs = (JSONArray) JSONObject.get("doubleConfig");

        for(Object o : standardConfigs){
            JSONObject jObj = (JSONObject) o;

            Macro macro = (Macro) this.standardConfig.get(((Long) jObj.get("key")).intValue() -1);
            macro.action = (String) jObj.get("action");
            macro.type = (String) jObj.get("type");
        }

        for(Object o : doubleConfigs){
            JSONObject jObj = (JSONObject) o;

            Macro macro = (Macro) this.doubleConfig.get(((Long) jObj.get("key")).intValue() -1);
            macro.action = (String) jObj.get("action");
            macro.type = (String) jObj.get("type");
        }
    }

    public void Set(int id, Macro standardMacro, Macro doubleMacro)
    {
        this.standardConfig.set(id, standardMacro);
        this.doubleConfig.set(id, doubleMacro);
    }

    public void SaveToConfigFile()
    {
        JSONObject jsObject = new JSONObject();

        JSONArray standardConfig = new JSONArray();
        JSONArray doubleConfig = new JSONArray();

        for (int i = 0; i < 16; i++){
            Macro standardMacro = (Macro) this.standardConfig.get(i);
            Macro doubleMacro = (Macro) this.doubleConfig.get(i);
            standardConfig.add(standardMacro.convertToJson());
            doubleConfig.add(doubleMacro.convertToJson());
        }

        jsObject.put("standardConfig", standardConfig);
        jsObject.put("doubleConfig", doubleConfig);

        //Write JSON file
        try (FileWriter file = new FileWriter("config.json")) {
            file.write(jsObject.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
