import java.io.IOException;

public class VolumeControl
{
    public static void setSystemVolume(int volume)
    {
        if(volume < 0 || volume > 100)
        {
            throw new RuntimeException("Error: " + volume + " is not a valid number. Choose a number between 0 and 100");
        }

        else
        {
            double endVolume = 655.35 * volume;

            Runtime rt = Runtime.getRuntime();
            Process pr;
            try
            {
                pr = rt.exec("nircmd.exe setsysvolume " + endVolume);
                pr = rt.exec("nircmd.exe mutesysvolume 0");

            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}