import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

public class MacroSettings {
    final JFileChooser fc = new JFileChooser();
    private Config config = new Config();
    private int selectedButton = 1;
    private JButton a10Button;
    private JButton a2Button;
    private JButton a11Button;
    private JButton a12Button;
    private JButton a13Button;
    private JButton a1Button;
    private JButton a3Button;
    private JButton a4Button;
    private JButton a5Button;
    private JButton a6Button;
    private JButton a7Button;
    private JButton a8Button;
    private JButton a9Button;
    private JButton a14Button;
    private JButton a15Button;
    private JButton a16Button;
    private JRadioButton textRadioButton;
    private JRadioButton keyCombinationRadioButton;
    private JRadioButton executableRadioButton;
    private JRadioButton textRadioButton1;
    private JRadioButton keyCombinationRadioButton1;
    private JRadioButton executableRadioButton1;
    private JTextField simpleTextField;
    private JTextField multiTextField;
    private JButton saveButton;
    private JPanel MainField;
    private JPanel ButtonArray;
    private JPanel SimpleSet;
    private JPanel MultiSet;
    private JPanel MultiRadioButtons;
    private JPanel SimpleRadioButtons;
    private JPanel MiscButtons;
    private JButton browseButtonSimple;
    private JButton browseButtonMulti;


    public MacroSettings() {
        applyMacroSettings();
        Driver driver = new Driver(config);
        simpleTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (keyCombinationRadioButton.isSelected()) {
                    AWTKeyStroke ak = AWTKeyStroke.getAWTKeyStrokeForEvent(e);
                    simpleTextField.setText(ak.toString());
                    config.standardConfig.set(selectedButton - 1, new Macro(selectedButton, ak.getModifiers() + "|" + ak.getKeyCode(), getStandardRadioChecked()));
                }
            }
        });

        multiTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (keyCombinationRadioButton1.isSelected()) {
                    AWTKeyStroke ak = AWTKeyStroke.getAWTKeyStrokeForEvent(e);
                    multiTextField.setText(ak.toString());
                    config.doubleConfig.set(selectedButton - 1, new Macro(selectedButton, ak.getModifiers() + "|" + ak.getKeyCode(), getDoubleRadioChecked()));
                }
            }
        });

        browseButtonSimple.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnVal = fc.showOpenDialog(MainField);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    simpleTextField.setText(file.getAbsolutePath());
                    config.standardConfig.set(selectedButton - 1, new Macro(selectedButton, file.getAbsolutePath(), "exePath"));
                }
            }
        });

        browseButtonMulti.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnVal = fc.showOpenDialog(MainField);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    multiTextField.setText(file.getAbsolutePath());
                    config.doubleConfig.set(selectedButton - 1, new Macro(selectedButton, file.getAbsolutePath(), "exePath"));
                }
            }
        });

        executableRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                browseButtonSimple.setEnabled(true);
            }
        });

        executableRadioButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                browseButtonMulti.setEnabled(true);
            }
        });

        ActionListener otherSimpleRadioButton = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                browseButtonSimple.setEnabled(false);
            }
        };
        ActionListener otherDoubleRadioButton = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                browseButtonMulti.setEnabled(false);
            }
        };

        ActionListener buttonEvent = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!keyCombinationRadioButton.isSelected()) {
                    Macro standardMacro = new Macro(selectedButton, simpleTextField.getText(), getStandardRadioChecked());
                    config.standardConfig.set(selectedButton - 1, standardMacro);
                }
                if (!keyCombinationRadioButton1.isSelected()) {
                    Macro doubleMacro = new Macro(selectedButton, multiTextField.getText(), getDoubleRadioChecked());
                    config.doubleConfig.set(selectedButton - 1, doubleMacro);
                }

                int newSelectedButton = Integer.parseInt(((JButton) e.getSource()).getText());
                selectedButton = newSelectedButton;
                setStandardRadioButton("text");
                setDoubleRadioButton("text");
                applyMacroSettings();
            }
        };

        ActionListener saveButtonEvent = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                save();
                JOptionPane.showMessageDialog(null,
                        "Saved successfully");
            }
        };

        keyCombinationRadioButton.addActionListener(otherSimpleRadioButton);
        textRadioButton.addActionListener(otherSimpleRadioButton);
        keyCombinationRadioButton1.addActionListener(otherDoubleRadioButton);
        textRadioButton1.addActionListener(otherDoubleRadioButton);

        saveButton.addActionListener(saveButtonEvent);

        a1Button.addActionListener(buttonEvent);
        a2Button.addActionListener(buttonEvent);
        a3Button.addActionListener(buttonEvent);
        a4Button.addActionListener(buttonEvent);
        a5Button.addActionListener(buttonEvent);
        a6Button.addActionListener(buttonEvent);
        a7Button.addActionListener(buttonEvent);
        a8Button.addActionListener(buttonEvent);
        a9Button.addActionListener(buttonEvent);
        a10Button.addActionListener(buttonEvent);
        a11Button.addActionListener(buttonEvent);
        a12Button.addActionListener(buttonEvent);
        a13Button.addActionListener(buttonEvent);
        a14Button.addActionListener(buttonEvent);
        a15Button.addActionListener(buttonEvent);
        a16Button.addActionListener(buttonEvent);
    }

    private void applyMacroSettings() {
        Macro standardMacro = (Macro) config.standardConfig.get(selectedButton - 1);
        Macro doubleMacro = (Macro) config.doubleConfig.get(selectedButton - 1);

        setStandardRadioButton(standardMacro.type);
        setDoubleRadioButton(doubleMacro.type);
        if (standardMacro.type.equals("keys")) {
            String[] keyCode = standardMacro.action.split("\\|");
            if (keyCode.length == 2) {
                simpleTextField.setText(AWTKeyStroke.getAWTKeyStroke(Integer.parseInt(keyCode[1]), Integer.parseInt(keyCode[0])).toString());
            }
        } else {
            simpleTextField.setText(standardMacro.action);
        }

        if (standardMacro.type.equals("exePath")) {
            browseButtonSimple.setEnabled(true);
        }

        if (doubleMacro.type.equals("keys")) {
            String[] keyCode = doubleMacro.action.split("\\|");
            if (keyCode.length == 2) {
                multiTextField.setText(AWTKeyStroke.getAWTKeyStroke(Integer.parseInt(keyCode[1]), Integer.parseInt(keyCode[0])).toString());
            }
        } else {
            multiTextField.setText(doubleMacro.action);
        }

        if (doubleMacro.type.equals("exePath")) {
            browseButtonMulti.setEnabled(true);
        }
    }

    private void save() {
        config.SaveToConfigFile();
    }

    public static void main(String[] args) {
        createGUIAndTrayIcon();
    }

    private static void createGUIAndTrayIcon() {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }

        JFrame frame = new JFrame("MacroSettings");
        frame.setContentPane(new MacroSettings().MainField);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        final PopupMenu popup = new PopupMenu();
        final TrayIcon trayIcon = new TrayIcon(Toolkit.getDefaultToolkit().getImage("Macro-icon.png"));
        final SystemTray tray = SystemTray.getSystemTray();

        // Create a popup menu components
        MenuItem aboutItem = new MenuItem("About");
        Menu displayMenu = new Menu("Display");
        MenuItem exitItem = new MenuItem("Exit");

        //Add components to popup menu
        popup.add(aboutItem);
        popup.addSeparator();
        popup.add(displayMenu);
        popup.add(exitItem);

        trayIcon.setPopupMenu(popup);

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
            return;
        }

        aboutItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "Made by: Benjamin Schaffrath, Niklas Kokoschka, Ralf Lemke");
            }
        });

        trayIcon.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!frame.isVisible()) {
                    frame.setVisible(true);
                }
            }
        });

        displayMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!frame.isVisible()) {
                    frame.setVisible(true);
                }
            }
        });

        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tray.remove(trayIcon);
                System.exit(0);
            }
        });
    }

    private void setStandardRadioButton(String text) {
        textRadioButton.setSelected(false);
        keyCombinationRadioButton.setSelected(false);
        executableRadioButton.setSelected(false);

        if (text.equals("text")) {
            textRadioButton.setSelected(true);
        } else if (text.equals("keys")) {
            keyCombinationRadioButton.setSelected(true);
        } else if (text.equals("exePath")) {
            executableRadioButton.setSelected(true);
        }
    }

    private void setDoubleRadioButton(String text) {
        textRadioButton1.setSelected(false);
        keyCombinationRadioButton1.setSelected(false);
        executableRadioButton1.setSelected(false);

        if (text.equals("text")) {
            textRadioButton1.setSelected(true);
        } else if (text.equals("keys")) {
            keyCombinationRadioButton1.setSelected(true);
        } else if (text.equals("exePath")) {
            executableRadioButton1.setSelected(true);
        }
    }

    public String getStandardRadioChecked() {
        String text = "";

        if (textRadioButton.isSelected()) {
            text = "text";
        } else if (keyCombinationRadioButton.isSelected()) {
            text = "keys";
        } else if (executableRadioButton.isSelected()) {
            text = "exePath";
        }

        return text;
    }

    public String getDoubleRadioChecked() {
        String text = "";

        if (textRadioButton1.isSelected()) {
            text = "text";
        } else if (keyCombinationRadioButton1.isSelected()) {
            text = "keys";
        } else if (executableRadioButton1.isSelected()) {
            text = "exePath";
        }

        return text;
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        MainField = new JPanel();
        MainField.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
        ButtonArray = new JPanel();
        ButtonArray.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 4, new Insets(0, 0, 0, 0), -1, -1));
        MainField.add(ButtonArray, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        a1Button = new JButton();
        a1Button.setFocusPainted(true);
        a1Button.setFocusable(true);
        a1Button.setInheritsPopupMenu(false);
        a1Button.setText("1");
        ButtonArray.add(a1Button, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a2Button = new JButton();
        a2Button.setText("2");
        ButtonArray.add(a2Button, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a3Button = new JButton();
        a3Button.setText("3");
        ButtonArray.add(a3Button, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a4Button = new JButton();
        a4Button.setText("4");
        ButtonArray.add(a4Button, new com.intellij.uiDesigner.core.GridConstraints(0, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a5Button = new JButton();
        a5Button.setText("5");
        ButtonArray.add(a5Button, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a6Button = new JButton();
        a6Button.setText("6");
        ButtonArray.add(a6Button, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a7Button = new JButton();
        a7Button.setText("7");
        ButtonArray.add(a7Button, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a8Button = new JButton();
        a8Button.setText("8");
        ButtonArray.add(a8Button, new com.intellij.uiDesigner.core.GridConstraints(1, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a9Button = new JButton();
        a9Button.setText("9");
        ButtonArray.add(a9Button, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a10Button = new JButton();
        a10Button.setText("10");
        ButtonArray.add(a10Button, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a11Button = new JButton();
        a11Button.setText("11");
        ButtonArray.add(a11Button, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a12Button = new JButton();
        a12Button.setText("12");
        ButtonArray.add(a12Button, new com.intellij.uiDesigner.core.GridConstraints(2, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a13Button = new JButton();
        a13Button.setText("13");
        ButtonArray.add(a13Button, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a14Button = new JButton();
        a14Button.setText("14");
        ButtonArray.add(a14Button, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a15Button = new JButton();
        a15Button.setText("15");
        ButtonArray.add(a15Button, new com.intellij.uiDesigner.core.GridConstraints(3, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        a16Button = new JButton();
        a16Button.setText("16");
        ButtonArray.add(a16Button, new com.intellij.uiDesigner.core.GridConstraints(3, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        SimpleSet = new JPanel();
        SimpleSet.setLayout(new BorderLayout(0, 0));
        MainField.add(SimpleSet, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Einfache Belegung");
        SimpleSet.add(label1, BorderLayout.NORTH);
        SimpleRadioButtons = new JPanel();
        SimpleRadioButtons.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 5, new Insets(0, 0, 0, 0), -1, -1));
        SimpleSet.add(SimpleRadioButtons, BorderLayout.CENTER);
        textRadioButton = new JRadioButton();
        textRadioButton.setSelected(true);
        textRadioButton.setText("Text");
        SimpleRadioButtons.add(textRadioButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        keyCombinationRadioButton = new JRadioButton();
        keyCombinationRadioButton.setText("Key Combination");
        SimpleRadioButtons.add(keyCombinationRadioButton, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        executableRadioButton = new JRadioButton();
        executableRadioButton.setText("Executable");
        SimpleRadioButtons.add(executableRadioButton, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        simpleTextField = new JTextField();
        SimpleRadioButtons.add(simpleTextField, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 3, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        SimpleRadioButtons.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(1, 4, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        browseButtonSimple = new JButton();
        browseButtonSimple.setEnabled(false);
        browseButtonSimple.setText("Browse");
        SimpleRadioButtons.add(browseButtonSimple, new com.intellij.uiDesigner.core.GridConstraints(1, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        MultiSet = new JPanel();
        MultiSet.setLayout(new BorderLayout(0, 0));
        MainField.add(MultiSet, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Doppelte Belegung");
        MultiSet.add(label2, BorderLayout.NORTH);
        MultiRadioButtons = new JPanel();
        MultiRadioButtons.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 5, new Insets(0, 0, 0, 0), -1, -1));
        MultiSet.add(MultiRadioButtons, BorderLayout.CENTER);
        textRadioButton1 = new JRadioButton();
        textRadioButton1.setSelected(true);
        textRadioButton1.setText("Text");
        MultiRadioButtons.add(textRadioButton1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        keyCombinationRadioButton1 = new JRadioButton();
        keyCombinationRadioButton1.setText("Key Combination");
        MultiRadioButtons.add(keyCombinationRadioButton1, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        executableRadioButton1 = new JRadioButton();
        executableRadioButton1.setText("Executable");
        MultiRadioButtons.add(executableRadioButton1, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer2 = new com.intellij.uiDesigner.core.Spacer();
        MultiRadioButtons.add(spacer2, new com.intellij.uiDesigner.core.GridConstraints(1, 4, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        multiTextField = new JTextField();
        MultiRadioButtons.add(multiTextField, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 3, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        browseButtonMulti = new JButton();
        browseButtonMulti.setEnabled(false);
        browseButtonMulti.setText("Browse");
        MultiRadioButtons.add(browseButtonMulti, new com.intellij.uiDesigner.core.GridConstraints(1, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        MiscButtons = new JPanel();
        MiscButtons.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        MainField.add(MiscButtons, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        saveButton = new JButton();
        saveButton.setText("Save");
        MiscButtons.add(saveButton, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer3 = new com.intellij.uiDesigner.core.Spacer();
        MiscButtons.add(spacer3, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        ButtonGroup buttonGroup;
        buttonGroup = new ButtonGroup();
        buttonGroup.add(textRadioButton);
        buttonGroup.add(keyCombinationRadioButton);
        buttonGroup.add(executableRadioButton);
        buttonGroup = new ButtonGroup();
        buttonGroup.add(textRadioButton1);
        buttonGroup.add(keyCombinationRadioButton1);
        buttonGroup.add(executableRadioButton1);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return MainField;
    }

}